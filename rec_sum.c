/*
write a reccursive function to calculate sum of digit of a number
*/

#include<stdio.h>
int sum_of_digit();
int main()
{
    int n,res;
    printf("Enter the number : ");
    scanf("%d", &n);
    res=sum_of_digit(n);
    printf("Sum Of Digits IS : %d\n",res);
}
int sum_of_digit(int n)
{   
    if(n>0)
    return sum_of_digit(n/10)+(n%10);
}