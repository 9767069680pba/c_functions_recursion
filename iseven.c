/*
. Write a function isEven(int) which accepts an integer n as the
   parameter and returns 1 if even else returns 0.
   Use this in main to accept n numbers and check whether they are even or
   odd.
   */

#include<stdio.h>
#include<stdlib.h>
int isEven();
int main()
{
    int i,n,*array;
    printf("\nHow Many Elements : ");
    scanf("%d", &n);
    array=(int *)malloc(n*sizeof(int));
    printf("\nEnter the %d Elements  : ",n);
    for(i=0;i<n;i++)
    {
        scanf("%d",array++);
    }
    for(i=0;i<n;i++)
    {
        //printf("%d\n",*array--);
        if(isEven(*--array)) printf("%d : EVEN\n",*array);
        else printf("%d : ODD\n",*array);
    }    
}
int isEven(int n)
{
    if(n%2==0) return 1;
    else return 0;
}