/*
Write a function to check whether the given number is prime or not
*/
#include<stdio.h>
_Bool isprime();
int main()
{
    int n;
    printf("Enter a Integer :  ");
    scanf("%i", &n);    
    if(n<1) 
    {
        printf("Enter Pure Natural Number......\n");
        return 1;
    }
    
    if(isprime(n)) printf("%i : PRIME\n",n);
    else printf("%i : COMPOSITE\n", n);
}
_Bool isprime(int n)
{
    int i=n/2;
    while(n%i!=0 && i>1)
    {
        i--;
    }
    return i==1;
}
