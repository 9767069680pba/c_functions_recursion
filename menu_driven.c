/*
4. Write a menu driven program to check whether a number is
   even, odd, prime, palindrome, Armstrong, perfect
*/

#include<stdio.h>
_Bool iseven(int n)
{
    return (n%2==0);
}
_Bool isodd(int n)
{
    return(n%2!=0);
}
_Bool isprime(int n)
{
    int i=n/2;
    while(n%i!=0 && i>1)
    {
        i--;
    }
    return i==1;
}
_Bool ispal(int n)
{
    int rev=0,temp=n,r;
    while(n>0)
    {
        r=n%10;
        rev=rev*10+r;
        n=n/10;
    }
    return (temp==rev);
}
_Bool isarm(int n)
{
   int arm=0,temp=n,r;
    while(n>0)
    {
        r=n%10;
        arm=arm+r*r*r;
        n=n/10;
    }
    return (temp==arm);    
}
_Bool isper(int n)
{
    int i,perfect=0;
    for(i=1;i<=n/2;i++)
    {
        if(n%i==0)
        perfect=perfect+i;
    }
    return (n==perfect);
}
 int main()
 {
     int n,op;
     
     printf("==================\n");
     printf("Enter Number : ");
     scanf("%i", &n);
     do
     {
     printf("\n-----MENU-----\n");
     printf("1)EVEN\n2)ODD\n3)PRIME\n4)PALINDROM\n5)ARMSTRONG\n6)PERFECT\n");
     printf("Enter the Option : ");
     scanf("%d", &op);
     switch(op)
     {
         case 1:
            if(iseven(n)) printf("Number Is Even\n");
            else printf("Number Is Not Even\n");
            break;
        case 2:
            if(isodd(n)) printf("Number Is Odd\n");
            else printf("Number Is Not Odd\n");
            break;
        case 3:
            if(isprime(n)) printf("Number Is Prime\n");
            else printf("Number Is Not Prime\n");
            break;
        case 4:
            if(ispal(n)) printf("Number Is Palindrom\n");
            else printf("Number Is Not Palindrom\n");
            break;
        case 5:
            if(isarm(n)) printf("Number Is Armstrong\n");
            else printf("Number Is Not Armstrong\n");
            break;
        case 6:
            if(isper(n)) printf("Number Is Perfect\n");
            else printf("Number Is Not Perfect\n");
            break;
        default:
            printf("Invalid Option Entered\n");
            break;
     }
     }while(op!=7);
 }