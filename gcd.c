/*
    gcd of Two number
*/
#include<stdio.h>
#define MAX(a,b) (a>b ? a:b)
int gcd();
int main()
{
    int a,b,res;
    printf("Enter the Two Number : ");
    scanf("%d%d", &a, &b);
    if(a==MAX(a,b))
        res=gcd(a,b);
    else
        res=gcd(b,a);
    printf("GCD of %d AND %d IS : %d\n",a,b,res);
}

int gcd(int a,int b)
{
    if(b==0)
    return a;
    else
    return gcd(b,a%b);
}