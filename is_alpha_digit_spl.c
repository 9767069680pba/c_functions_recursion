/*
 Write a function which accepts a character and returns
   1 if alphabet
   2 if digit
   3 if special character

   In main accept char up to EOF and use a function to count the number of
   alpha, digits and special chars
   */
#include<stdio.h>
#include<ctype.h>
int check(char);
int main()
{
    char ch;
    int count[3]={0,0,0};
    while(ch!=EOF)
    {
        ch=getchar();
        switch(check(ch))
        {
            case 1:
                    printf("\t%c : Alphabet\n",ch);
                    count[0]++;
                    break;
            case 2:
                    printf("\t%c : Digit\n",ch);
                    count[1]++;
                    break;
            case 3:
                    printf("\t%c : Special Character\n",ch);
                    count[2]++;
                    break;
        }
        
        }
            printf("\nAlphabates : %d \nDigits : %d\nSpecial Charactyers : %d\n",count[0],count[1],count[2]);
}
int check(char ch)
{
    if(isalpha(ch)) return 1;
    else if(isdigit(ch)) return 2;
    return 3;  
}